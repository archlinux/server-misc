#!/bin/bash

. $HOME/backup.conf || exit 1

LOGFILE="/tmp/backup.log"
: >$LOGFILE

err() {
	echo "$*" >&2
	exit 1
}

log() {
	echo "$*" >>$LOGFILE
	echo "$*"
}

rotatefile() {
	i=$COPIES
	while [ $i -gt 0 ]; do
		if [ $i -eq $COPIES ]; then
			[ -f ${1}.${i} ] && rm -f ${1}.${i}
		else
			[ -f ${1}.${i} ] && mv ${1}.${i} ${1}.$((i+1))
		fi
		i=$((i-1))
	done
	[ -f $1 ] && mv $1 ${1}.1
}

# make sure our peers are alive
for peer in ${PEERS[@]}; do
	ping -c 2 $peer &>/dev/null
	if [ $? -gt 0 ]; then
		err "Backup peer '$peer' is not responding!"
	fi
done

# got dirs?
if [ ! -d $BACKUPDIR/local ]; then
	mkdir $BACKUPDIR/local || exit 1
fi
cd $BACKUPDIR/local

# ssh config
sshconf=
if [ -f "$SSHCONFIG" ]; then
    sshconf="-F $SSHCONFIG"
fi

# tar exclude file
ex=
if [ -f $HOME/backup.excludes ]; then
	ex="--exclude-from $HOME/backup.excludes"
fi

#
# BACKUP FILES
#
for dir in ${DIRS[@]}; do
	arcname=$(echo $dir | sed 's|^/||' | sed 's|/|#|g')
	rotatefile ${arcname}.tar.bz2
	log "tarring $dir to $(pwd)/${arcname}.tar.bz2"
	tar cjf ${arcname}.tar.bz2 $ex $dir 2>/dev/null
	# tough to error check here, tar returns non-zero if a file
	# changed as it was being read, which happens often

	# send to peers
	for peer in ${PEERS[@]}; do
		log "sending ${arcname}.tar.bz2 to $peer"
		cat ${arcname}.tar.bz2 | ssh -T $sshconf ${peer} ${arcname}.tar.bz2 -t${COPIES}
	done
done

#
# BACKUP DATABASES
#
for db in ${DBS[@]}; do
	rotatefile db-${db}.sql.bz2
	log "dumping $db to $(pwd)/db-${db}.sql.bz2"
	if [ "$db" = "all" ]; then
		mysqldump -A -u $DBUSER -p$DBPASS | bzip2 >db-${db}.sql.bz2
	else
		mysqldump -u $DBUSER -p$DBPASS $db | bzip2 >db-${db}.sql.bz2
	fi

	# send to peers
	for peer in ${PEERS[@]}; do
		log "sending db-${db}.sql.bz2 to $peer"
		cat db-${db}.sql.bz2 | ssh -T $sshconf ${peer} db-${db}.sql.bz2 -t${COPIES}
	done
done

log
log "Disk Usage:"
du -ch $BACKUPDIR/local | tee -a $LOGFILE

if [ "$MAILTO" != "" ]; then
	mail -s "Backup Report for $(hostname)" $MAILTO <$LOGFILE
fi

rm -f $LOGFILE

exit 0
